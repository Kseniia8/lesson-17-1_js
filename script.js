document.addEventListener('DOMContentLoaded', () => {
    const changeTextButton = document.getElementById('change-text-button');
    const messageDiv = document.getElementById('message');

    changeTextButton.addEventListener('click', () => {
        setTimeout(() => {
            messageDiv.textContent = "Операція виконана успішно";
        }, 3000);
    });
});


document.addEventListener('DOMContentLoaded', () => {
    const countdownDiv = document.getElementById('countdown');
    let countdownValue = 10;

    const countdownInterval = setInterval(() => {
        countdownDiv.textContent = countdownValue;
        countdownValue--;

        if (countdownValue < 0) {
            clearInterval(countdownInterval);
            countdownDiv.textContent = "Зворотній відлік завершено";
        }
    }, 1000);
});
